## Create file assignment

1. Read the readme - thats me :)
1. Git clone the project
1. Make script, executable - `chmod +x ./run.py`
1. Test run the script - `python3 ./run.py`
    1. Script should output "Code goes here... have fun!"
1. Update the script, so that the script creates a file named `data.txt` and write 3 lines of "Barca Rules" in the data.txt file.
    1. Help can be found here - https://pynative.com/python/file-handling/
    1. and here - https://www.stechies.com/python-insert-new-line-into-string/
